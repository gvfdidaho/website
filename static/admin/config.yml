backend:
  name: git-gateway
  repo: gvfdidaho/website
  branch: master
media_folder: "static/media" # Folder where user uploaded files should go
public_folder: "/media"

title: 'Garden Valley Fire Department | Idaho'
logo_url: 'https://gvfdidaho.gitlab.io/website/media/logo_gardenvalleyfire-badge.png'

relativeURLs: true
buildFuture: true


collections: # A list of collections the CMS should be able to edit
 - name: 'settings'
   label: 'Settings'
   delete: false 
   editor:
     preview: false
   files:
      - name: 'general'
        label: 'Site Settings'
        file: 'data/settings.json'
        description: 'General Site Settings'
        fields:
          - {label: "Favicon",name: "favicon", widget: "image", choose_url: false, default: "", allow_multiple: false }
          - label: 'Byline'
            name: byline
            widget: 'object'
            fields:
              - { label: 'Byline', name: byline_text, widget: string }
              - { label: 'Display', name: byline_display, widget: "select", options: ["all", "home", "none"] }
          - label: 'Show on homepage'
            name: 'hp_counts'
            widget: 'object'
            fields:
              - { label: 'News', name: news_count, widget: number, min: 0, max: 5, default: 3, }
              - { label: 'Events', name: events_count, widget: number, min: 0, max: 5, default: 3, }

# 'Static' Pages
 - name: 'section_pages'
   label: 'Section Pages'
   delete: false
   editor:
     preview: false
   files:
      - name: 'about'
        label: 'About Page'
        file: 'data/about.json'
        description: 'About Page'
        fields:
          - name: 'about_group'
            label: 'About Page'
            widget: object
            fields:
              - { label: 'Page Title', name: 'about_title', widget: 'string', hint: 'About GVFD' }
              - { label: 'Page Caption', name: 'about_caption', widget: 'text' }
              - { label: 'Detail Title', name: 'detail_title', widget: 'string' }
              - { label: 'Detail Body', name: 'detail_body', widget: 'markdown' }       
          - name: 'link_group'
            label: 'Three Links'
            widget: object
            fields:
              - { label: 'People Blurb', name: 'people_blurb', widget: 'string' }              
              - { label: 'Assets Blurb', name: 'assets_blurb', widget: 'string' }
              - { label: 'Services Blurb', name: 'services_blurb', widget: 'string' }  

      - name: 'aboutPeople'
        label: 'About > People Page'
        file: 'data/people.json'
        description: 'People Page'
        fields:
          - name: 'people_group'
            label: 'People Page'
            widget: object
            fields:
              - { label: 'Commissioners', name: 'commissioners_caption', widget: 'text', required: false, hint: "Optional paragraph" } 
              - { label: 'Officers', name: 'officers_caption', widget: 'text', required: false, hint: "Optional paragraph"  }
              - { label: 'Volunteers', name: 'volunteers_caption', widget: 'text', required: false, hint: "Optional paragraph"  }
              - { label: 'Auxiliary', name: 'auxiliary_caption', widget: 'markdown', required: false, hint: "Optional paragraph", buttons: ["bold","italic","quote","link"],editor_components: [] }

      - name: 'aboutAssets'
        label: 'About > Assets Page'
        file: 'data/assets.json'
        description: 'Assets Page'
        fields:
          - name: 'assets_group'
            label: 'Assets Page'
            widget: object
            fields:
              - { label: 'Stations', name: 'stations_caption', widget: 'text', required: false, hint: "Optional paragraph" } 
              - { label: 'Apparatus', name: 'apparatus_caption', widget: 'text', required: false, hint: "Optional paragraph"  }
              - { label: 'Equipment', name: 'equipment_caption', widget: 'text', required: false, hint: "Optional paragraph"  }

      - name: 'aboutServices'
        label: 'About > Services Page'
        file: 'data/services.json'
        description: 'Services Page'
        fields:
          - name: 'services_group'
            label: 'Services Page'
            widget: object
            fields:
              - { label: 'Services', name: 'services_caption', widget: 'text', required: false, hint: "Optional paragraph" } 

      - name: 'resources'
        label: 'Resources Page'
        file: 'data/resources.json'
        description: 'Resources Page'
        fields:
          - { label: 'Resource Body', name: 'resource_body', widget: 'markdown' } 
          - name: resources_links
            label: Links
            label_singular: 'Link'
            widget: list
            fields:
              - {label: "Display Text", name: "links_text", widget: string }
              - {label: "Link URL", name: "links_url", widget: file, allow_multiple: false }
          - name: resources_documents
            label: Documents
            add_to_top: true
            label_singular: 'Document'
            widget: list
            fields:
              - {label: "Display Text", name: "docs_text", widget: string }
              - {label: "Link URL", name: "docs_url", widget: file, allow_multiple: false }
          - name: resources_public_info
            label: Public Info
            label_singular: 'Info'
            widget: list
            fields:
              - {label: "Display Text", name: "info_text", widget: string }
              - {label: "Link URL", name: "info_url", widget: file, allow_multiple: false, }

# Landing Pages
 - name: 'landing_pages'
   label: 'Landing Pages'
   label_singular: 'Page'
   folder: 'content/pages'
   create: true
   delete: true
   fields:
      - { label: "Title", name: "title", widget: "string", } 
      - { label: 'Date', name: 'date', widget: 'datetime', }
      - { label: "Feature Image", name: "image",widget: "image",choose_url: false,default: "",allow_multiple: false, required: false,}
      - { label: "Is Quicklink?", name: "quicklink", widget: boolean, hint: 'Toggles whether this page shows footer.'}
      - { label: "Body", name: "body", widget: "markdown", }     


# Home Hero
 - name: 'home'
   label: 'Home Page Heros'
   label_singular: 'Hero'
   folder: 'content/heros'
   create: true
   fields:
      - { label: "Title", name: "title", widget: "string", } 
      - { label: 'Date', name: 'date', widget: 'datetime', picker_utc: true }
      - {
          label: "Hero Image",
          name: "hero_image",
          widget: "image",
          choose_url: false,
          default: "",
          allow_multiple: false,
        }
      - { label: "Link URL",
              name: "hero_link", 
              widget: "string",
              default: "www.gvfdidaho.com",
            }
      - { label: "Call-to-Action Text", name: "cta_text", widget: "text", hint: "Keep it short, 4-6 words."}  
      - { label: 'Text Color', name: 'cta_text_color', widget: 'color', enableAlpha: true, }
      - { label: "Text Placement", name: "cta_align", widget: "select", options: ["left", "center", "right"] }              
      - { label: 'Button Color', name: 'btn_bg_color', widget: 'color', enableAlpha: true,  }
      - { label: 'Button Text Color', name: 'btn_text_color', widget: 'color', enableAlpha: true, }
      - { label: "Button Text", name: "cta_btn_text", widget: "string", }     

 - name: 'run_stats' # Used in routes, ie.: /admin/collections/:slug/edit
   label: 'Run Stats' # Used in the UI
   label_singular: 'Month' # Used in the UI, ie: "New Post"
   folder: 'content/stats'
   slug: '{{date}}'
   summary: "{{date | date('YYYY-MM')}}"
   sortable_fields: ['date']
   create: true # Allow users to create new documents in this collection
   fields: # The fields each document in this collection have
      - { label: 'Title',name: 'title',widget: 'hidden',tagname: 'h1',default: "Monthly Run Stats for {{date | date('YYYY-MM')}}"}
      - { label: 'Stat Date', name: 'date', widget: 'datetime', picker_utc: true, hint: 'Pick any day of the month.'}    
      - { label: 'Medical', name: 'stat_medical', widget: 'number', value_type: "int", min: 0, max: 100, step: 1, default: 5 }
      - { label: 'Fire', name: 'stat_fire', widget: 'number', value_type: "int", min: 0, max: 100, step: 1, default: 5 }
      - { label: 'Other', name: 'stat_other', widget: 'number', value_type: "int", min: 0, max: 100, step: 1, default: 5 }


#News aka blog aka posts              
 - name: 'news' # Used in routes, ie.: /admin/collections/:slug/edit
   label: 'News' # Used in the UI
   label_singular: 'Article' # Used in the UI, ie: "New Post"
   description: >
     Put a link to style and brand guide.
   folder: 'content/news'
   slug: '{{year}}-{{month}}-{{day}}-{{slug}}'
   summary: '{{title}} -- {{year}}/{{month}}/{{day}}'
   create: true # Allow users to create new documents in this collection
   view_filters:
      - label: Posts With Index
        field: title
        pattern: 'This is post #'
      - label: Drafts
        field: draft
        pattern: true
   view_groups:
      - label: Year
        field: date
        pattern: \d{4}
      - label: Drafts
        field: draft
   fields: # The fields each document in this collection have
      - { label: 'Title', name: 'title', widget: 'string', tagname: 'h1' }
      - { label: 'Draft', name: 'draft', widget: 'boolean', default: false }
      - {
          label: 'Publish Date',
          name: 'date',
          widget: 'datetime',
          picker_utc: true,
          date_format: 'YYYY-MM-DD',
          time_format: 'HH:mm',
        }
      - { label: 'Body', name: 'body', widget: 'markdown', hint: 'Main content goes here.' }

 - name: 'events' # Used in routes
   label: 'Events' # Used in the UI
   label_singular: 'Event' 
   description: >
      Put a link to style and brand guide.
   folder: 'content/events'
   slug: "{{slug}}-{{year}}{{month}}{{day}}"
   summary: "{{title}} -- {{year}}{{month}}{{day}}"
   create: true # Allow users to create new documents in this collection
   view_groups:
      - label: Year
        field: date
        pattern: \d{4}
      - label: Drafts
        field: draft
   fields: # The fields each document in this collection have
      - { label: 'Title', name: 'title', widget: 'string', tagname: 'h1' }
      - { label: 'Draft', name: 'draft', widget: 'boolean', default: false }
      - { label: 'Event Date', name: 'date', widget: 'datetime', picker_utc: true, }  
      - { label: 'Body', name: 'body', widget: 'markdown', hint: 'Main content goes here.' }

#People
 - name: 'commissioners'
   label: 'People - Commissioners'
   label_singular: 'Commissioner'
   folder: 'content/people/commissioners'
   create: true
   fields:
          - { label: 'Name', name: 'title', widget: 'string', hint: 'First and Last' }
          - { label: 'Term Expires', name: 'date', widget: 'datetime', picker_utc: true, }
          - { label: 'Email Address', name: 'email', widget: 'string' }
          - { label: 'Subdistrict', name: 'subdistrict', widget: 'number', value_type: "int", min: 1,  max: 3, step: 1 }
          - { label: 'Picture', name: 'image', widget: "image", choose_url: false, allow_multiple : false, required: false }
          - { label: "Type", name: "type", widget: "hidden", default: "commissioner"}
          - { label: 'Bio', name: 'body', widget: 'markdown', required: false }

 - name: 'volunteers'
   label: 'People - Volunteers'
   label_singular: 'Volunteer'
   folder: 'content/people/volunteers'
   summary: '{{rank}} {{title}}'
   create: true
   sortable_fields: ['title', 'rank']
   view_filters:
     - label: Active
       field: active
       pattern: true
     - label: Inactive
       field: active
       pattern: false
   view_groups:
     - label: Section
       field: layout
   fields:
     - { label: 'Name', name: 'title', widget: 'string', hint: 'First and Last' }
     - { label: "Active", name: "active", widget: "boolean", default: true, hint: 'Only active volunteers will show on the site.' }
     - { label: 'Start Date', name: 'date', widget: 'datetime', picker_utc: true, hint: 'Used to calculate length of service' }
     - { label: "Certifications", name: "badge", widget: "relation", multiple: true, collection: "certifications", search_fields: ["title"], value_field: "badge" }
     - { label: 'Rank', name: 'rank', widget: 'select', options: ["1. Chief", "2. Battalion Chief", "3. Captain", "4. Lieutenant", "7. Chaplain", "9. None"], hint:"Sorting, numbers won't display" }
     - { label: 'Section', name: 'layout', widget: 'select', options: ["officer", "volunteer" ], hint: "Grouping, section to place person." }
     - { label: "Type", name: "type", widget: "hidden", default: "volunteer"}
     - { label: 'Email Address', name: 'email', widget: 'string', required: false }
     - { label: 'Picture', name: 'image', widget: "image", choose_url: false, allow_multiple: false, required: false, hint: "Optional" }
     - { label: 'Bio', name: 'body', widget: 'markdown', required: false, hint: "Optional", buttons: ["bold","italic","quote"],editor_components: [] }

          
 - name: 'auxiliary'
   label: 'People - Auxiliary'
   label_singular: 'Member'
   folder: 'content/people/auxiliary'
   create: true
   fields:
     - { label: 'Name', name: 'title', widget: 'string', hint: 'First and Last' }
     - { label: 'Title', name: 'rank', widget: 'string', hint: "President, VP, etc" }
     - { label: "Type", name: "type", widget: "hidden", default: "auxiliary"}
     - { label: 'Email Address', name: 'email', widget: 'string', required: false, pattern: ["[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", "Must be a valid email address."] }

# Assets     
 - name: 'stations'
   label: 'Assets - Stations'
   label_singular: 'Station'
   folder: 'content/assets/stations'
   create: true
   fields:
     - { label: 'Name', name: 'title', widget: 'string' }
     - { label: 'Street Address', name: 'street_address', widget: 'string', hint: 'Street only. Garden Valley, ID 83622 is assumed.'}
     - { label: 'Picture', name: 'image', widget: "image", choose_url: false, allow_multiple : false, hint: '640 x 480'}
     - { label: "Type", name: "type", widget: "hidden", default: "station"}
     - { label: 'Body', name: 'body', widget: 'markdown', required: false }

 - name: 'apparatus'
   label: 'Assets - Appparatus'
   label_singular: 'Appparatus'
   folder: 'content/assets/apparatus'
   create: true
   fields:
     - { label: 'Name', name: 'title', widget: 'string' }
     - { label: 'Year', name: 'year', widget: 'number', default: 2010, min: 1980, max: 2050, step: 1 }
     - { label: 'Picture', name: 'image', widget: "image", choose_url: false, allow_multiple : false, hint: '720 x 480' }
     - { label: "Type", name: "type", widget: "hidden", default: "apparatus"}
     - { label: 'Body', name: 'body', widget: 'text', hint: '112 character max', pattern: ['^.{1,112}$', "112 character max"] }
          
 - name: 'equipment'
   label: 'Assets - Equipment'
   label_singular: 'Equipment'
   folder: 'content/assets/equipment'
   create: true
   fields:
     - { label: 'Name', name: 'title', widget: 'string' }
     - { label: 'Picture', name: 'image', widget: "image", choose_url: false, allow_multiple : false, hint: '720 x 480' }
     - { label: "Type", name: "type", widget: "hidden", default: "equipment"}
     - { label: 'Body', name: 'body', widget: 'text', hint: '112 character max', pattern: ['^.{1,112}$', "112 character max"] }

 - name: 'services'
   label: 'Services'
   label_singular: 'Service'
   folder: 'content/services'
   create: true
   fields:
     - { label: 'Name', name: 'title', widget: 'string' }
     - { label: 'Picture', name: 'picture', widget: "image", choose_url: false, allow_multiple : false, hint: '720 x 480' }
     - { label: 'Description', name: 'body', widget: 'markdown' }


 - name: 'certifications'
   label: 'Certifications'
   label_singular: 'Certification'
   folder: 'content/certifications'
   create: true
   fields:
     - { label: 'Full Name', name: 'title', widget: 'string', hint: 'Human ' }
     - { label: 'Short Name', name: 'badge', widget: 'string', pattern: ['^[a-z]+$', "Lowercase letters only"], hint: 'lowercase letters only' }
     - { label: 'Color', name: 'color', widget: 'color', default: '#16191C' }
     - { label: 'Icon', name: 'icon', widget: "image", choose_url: false, allow_multiple: false, hint: "White with tranparent background, 32x32 pixels." }
     - { label: 'Description', name: 'body', widget: 'markdown', required: false }
 
