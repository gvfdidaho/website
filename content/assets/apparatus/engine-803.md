---
title: Engine 803
year: 2005
image: media/assets_apparatus_engine-803.jpg
type: apparatus
---
The flagship engine of GVFD, 803 is the engine that responds to structure fires. 