---
title: Engine 841
year: 2005
image: media/assets_apparatus_engine-841.jpg
type: apparatus
---
This type six wildland engine is first out to get boots on the ground and eyes on the fire.