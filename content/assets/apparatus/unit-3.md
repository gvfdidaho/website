---
title: Unit 3
year: 2006
image: /media/assets_apparatus_unit-2.jpg
type: apparatus
---
A fully equipped backup ambulance used for Event Standby or in the event of multiple calls or patients.