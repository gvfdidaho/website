---
title: Engine 842
year: 2017
image: media/assets_apparatus_engine-842.jpg
type: apparatus
---
Our main wildland rig can get 5 firefighters and all their gear close to the fire line.