---
title: August 2023 Training
draft: false
date: 2023-09-06T14:16:19.407Z
---
August training spanned the spectrum of from hands-on dirty work to new technology. 

<!--more-->

Fire training included stabilization and extrication. The EMS side reviewed patient assessments and Nitrous Oxide administration. We also conducted AARs, and training and scenarios for using electronic PCRs.