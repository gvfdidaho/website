---
title: June 2023 Training
draft: false
date: 2023-07-07T14:02:12.303Z
---
June training included: After Action Reviews, Wildland training, pumping and engine operations, hose lays and pulls, EMS scenarios, medical reviews, and a recognition and awards presentation.