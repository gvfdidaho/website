---
title: 2023 November Training
draft: false
date: 2023-12-02T06:45:37.042Z
---
November was a slow months for calls which helped us train on a wide variety of skills to prepare for winter.

<!--more-->

Training included: new ePCRs on the Toughbooks, Hep B vaccines, hypothermia and anaphylaxis, optional Epi module, timed turnout donning, tire chains, SCBA training, new hose lays on the engines, stokes basket rescue, and knot refresher.