---
title: July 2023 Training
draft: false
date: 2023-08-02T17:10:02.614Z
---
July training blended Fire and EMS. We focused on two specialized areas: swift water rescue and extrication.
<!--more-->
With summer in full swing, Garden Valley is seeing our yearly influx of visitors. Increased traffic sadly means an increase in traffic accidents, and with more rafters on the rivers, an increase in swift water rescue calls.

We had two training classes dedicated to swift water rescue. Donning full rescue suits and PPE we performed rescue scenarios in the South Fork of the Payette. Our training was quickly and thoroughly tested by a complex rescue in the canyon section of the South Fork near Lowman. The training paid off and all victims and rescuers returned safe.

Vehicle extrication is the work of removing a vehicle from around a person who has been involved in an accident. Extrication must be performed quickly and safely in a chaotic environment. Traffic, hazard mitigation, fire, and medical care must all be provided during extrication. 

In addition, we had multiple AARs (after action reviews) of some of our more difficult calls and also practiced driving and pumping.

July was a busy month.
