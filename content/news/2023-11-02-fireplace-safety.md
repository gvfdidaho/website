---
title: Fireplace Safety
draft: false
date: 2023-11-02T05:32:13.308Z
---
## Fireplace Safety

Winter is near, and with its arrival, the joy of a roaring fire. Take the proper steps to make sure you are not curling up next to a potential fire hazard. Nobody intends to have an out-of-control fire, and nobody thinks it will happen to them. But fires happen every single day.

Last winter GVFD responded to 25 fire calls. Not all were not fully ingulfed house fires. Some were chimney fires, electrical fires, carbon monoxide alarms, or secondary fires from dumping hot ashes. Please take a few minutes to minimize your risks.

### Before you Burn

Make sure your chimney is in proper working order. Your chimney should be cleaned yearly. Soot and carbon are unburnt fuels that can ignite in your chimney. They can also block airflow, pushing deadly carbon monoxide into your home.

Clean the ashes after each burn. Make sure to place the ashes into a fireproof metal bucket with a tight-fitting lid.

### Burning

Start a fire with newspaper or dedicated fire-starters. Do not use trash, plastics, or heavily inked paper. While they may work, they let off toxic fumes. 

Burn only natural wood that has seasoned for at least six months. Do not burn plywood, or wood that has been painted or stained.

The hotter you burn; the less un-combusted fuel is left to block your chimney. A slower burn will decrease the amount of wood you use but will require you to clean your chimney more often.

Never leave a fire unattended.

### Must-haves

Smoke detectors. Swap the batteries yearly and make sure they are working. At least 1 on each level of the house, 1 in each sleeping area, and 1 outside sleeping areas (hall leading to bedrooms).

Carbon Monoxide detectors. Carbon monoxide is gas that has no odor, taste, or color. It is undetectable by humans but deadly. Symptoms include drowsiness, confusion, and weakness. All the more reason to detect the threat before it impairs your ability to react. At least 1 on every level, and 1 near any fuel-burning appliance or open fire.

Fire extinguisher. A home fire extinguisher should be an ABC-rated extinguisher. These extinguishers put out regular, electrical, and gas/liquid fires. 

### What to do if...

A carbon monoxide detector goes off or you feel symptoms (headache, tired, confused) of carbon monoxide poisoning: Exit the house immediately, no exceptions. Call 911 from outside or from a neighbor’s house.

You witness a fire get out of control: First call 911. If you have an extinguisher AND are proficient in its use AND feel comfortable attempting to put out the fire- try but have a clear escape route and don't hesitate to use it. Never risk life for property!

Smoke detectors activate but you don't see a fire: exit the house and call 911 from outside. A fire may be burning within the walls, attic, or crawl space. As smoke accumulates and temperatures rise a flashover can occur, instantly ingulfing the house.

If ever in doubt, call 911, and call early. This is the purpose of the fire department. Even if you get the fire under control, we can test the air for gases and use our TIC (thermal imaging camera) to look for hotspots that are not visible. Burn safe and stay safe.