---
title: 2025 January Training
draft: false
date: 2024-12-31T17:10:02.614Z
---
Your Fire Department continues to train and educate your volunteer's with a training program of firefighter essentials.  This training is occurring weekends (Friday, Saturday & Sunday) in January, the dedication these volunteers continue to do in support of the community.

![Training members in front of station 1](/media/training3.jpg)
