---
title: New Landing Zone
date: 2022-12-25
hero_image: /media/hero-newlz.jpg
hero_link: https://www.gvfdidaho.com/news/2022-12-10-new-landing-zone/
cta_text: New Community Church Landing Zone
cta_text_color: "#ccdbdd"
cta_align: left
btn_bg_color: "#fdfdfd"
btn_text_color: "#0a2c86"
cta_btn_text: New LZ
---
