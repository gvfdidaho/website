---
title: Volunteer Turnout
date: 2023-08-02
hero_image: /media/turnouts.jpg
hero_link: https://www.gvfdidaho.com/news/2023-08-02-the-recruitment-process-is-open/
cta_text: |-
  Volunteers needed
  turnout today.
cta_text_color: "#ffffff"
cta_align: center
btn_bg_color: "#0a0a0a"
btn_text_color: "#ffffff"
cta_btn_text: Join us
---
