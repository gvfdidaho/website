---
title: Fireplace Safety
date: 2023-11-03T13:38:37.662Z
hero_image: /media/851cb7ed-c8f4-4b16-8175-1ec1ff0c56c4.jpeg
hero_link: https://www.gvfdidaho.com/news/2023-11-02-fireplace-safety/
cta_text: Fire - friend and foe.
cta_text_color: "#f4f2f2"
cta_align: center
btn_bg_color: "#1c1001"
btn_text_color: "#f0eeee"
cta_btn_text: Burn Safe
---
