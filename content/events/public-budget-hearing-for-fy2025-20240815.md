---
title: Public Budget Hearing for FY2025
draft: false
date: 2024-08-26T14:00:29.329Z
---
Notice is Hereby given that the Garden Valley Fire Protection
District Board of Commissioners will be holding our Public
Budget Hearing for FY2025 on August 26th, 2024 at 2PM at our
Main Station located at 373 S. Middlefork Road, Garden Valley,
Idaho.

[Public Budget Hearing for FY2025 (pdf)](https://www.gvfdidaho.com/media/gvfpd-budget-fy2025-for-online-publication.pdf)