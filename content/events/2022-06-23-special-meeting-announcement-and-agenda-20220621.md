---
title: 2022-06-23 Special Meeting Announcement and Agenda
draft: false
date: 2022-06-23T08:00:38.989Z
---
Special Meeting (Executive Session Only)

<!--more-->

[Announcement and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-23-special-meeting-exec-session-announcement-and-agenda.pdf)