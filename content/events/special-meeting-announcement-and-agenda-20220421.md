---
title: Special Meeting Announcement and Agenda
draft: false
date: 2022-04-26T14:00:34.340Z
---
The Garden Valley Fire Protection District Board of Commissioners will be holding a Special Meeting on Tuesday, 04/26. The meeting will be held at the main station, 373 Middlefork Rd, at 2pm.

Topics include: Budget Preparations, Website Support and Initial Funding, and an executive session to review Fire Chief applications.

For more information see the Announcement and Agenda on our [Resources](https://www.gvfdidaho.com/resources/) page.