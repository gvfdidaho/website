---
title: GVFPD Board of Commissioners Special Meeting 2022-06-01
draft: false
date: 2022-06-01T15:00:41.501Z
---
Special Meeting Announcements and Agenda.

<!--more-->[](https://www.gvfdidaho.com/media/2022-06-01-special-meeting-exec-session-announcement-and-agenda.pdf)

[Announcement and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-01-special-meeting-exec-session-announcement-and-agenda.pdf)