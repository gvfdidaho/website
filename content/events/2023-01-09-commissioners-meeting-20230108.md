---
title: 2023-01-09 Commissioners Meeting
draft: false
date: 2023-01-09T14:00:07.379Z
---
The GVFPD Fire Commissioners monthly meeting on January 9, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 09 January 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-01-09-commagendapdf.pdf)