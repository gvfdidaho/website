---
title: 2023-05-16 Commissioners Meeting
draft: false
date: 2023-05-16T14:00:31.157Z
---
The GVFPD Fire Commissioners monthly meeting originally scheduled for 05/09 is now on May 16, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 16 May 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-05-16_agenda.pdf)