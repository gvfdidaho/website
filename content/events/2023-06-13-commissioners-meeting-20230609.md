---
title: 2023-06-13 Commissioners Meeting
draft: false
date: 2023-06-13T14:00:56.283Z
---
The GVFPD Fire Commissioners meeting is scheduled for June 13, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 13 June 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-06-13-commissionersmeetingagenda.pdf)