---
title: 2022-08-11 Meeting Announcement & Agenda
draft: false
date: 2022-08-11T14:00:00.716Z
---
The GVFPD Board of Commissioners regular meeting will be held at 2pm at the main fire station (373 S Middlefork Rd). Rescheduled to this date from 9 August due to Budget Hearing on that day.

[Full Agenda (pdf)](https://www.gvfdidaho.com/media/20220805092357279.pdf)