---
title: Special Meeting Announcements and Agenda
draft: false
date: 2022-06-08T08:00:53.857Z
---
Special Meeting Announcements and Agenda
Executive Session

<!--more-->

[Announcements and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-08-special-meeting-exec-session-announcement-and-agenda.pdf)