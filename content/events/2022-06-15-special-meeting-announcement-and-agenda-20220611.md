---
title: 2022-06-15 Special Meeting Announcement and Agenda
draft: false
date: 2022-06-15T15:00:39.294Z
---
Special Meeting Announcement and Agenda.

Where: Pep Building at St. Jude Church in Garden Valley

When: June 15, 2022, 3 PM

<!--more-->

[Announcement and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-15-special-meeting-exec-session-announcement-and-agenda.pdf)