---
title: 2023-12-12 Commissioners Meeting
draft: false
date: 2023-12-12T14:00:07.109Z
---
The GVFPD Fire Commissioners meeting is scheduled for December 12, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 12 December 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-12-12-regular-meeting-agenda.pdf)