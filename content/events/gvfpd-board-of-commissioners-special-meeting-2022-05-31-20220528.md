---
title: GVFPD Board of Commissioners Special Meeting 2022-05-31
draft: false
date: 2022-05-31T14:00:37.317Z
---
Garden Valley Fire Protection District Board of Commissioners will be holding a 
Special Meeting at the main fire station (373 Middlefork Rd) at 2pm on 05/31.

<!--more-->[](https://www.gvfdidaho.com/media/2022-05-31-special-meeting-announcement-and-agenda.pdf)

[Announcement and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-05-31-special-meeting-announcement-and-agenda.pdf)