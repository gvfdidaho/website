---
title: 2023-08-30 Special Meeting
draft: false
date: 2023-08-30T14:00:29.358Z
---
Garden Valley Fire Protection District Board of Commissioners
Special Meeting / Executive Session Announcement & Agenda

<!--more-->

Where: Main Fire Station on Middlefork Rd above Crouch
373 Middlefork Rd

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-08-30-special-meeting-agenda.pdf)