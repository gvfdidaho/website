---
title: 2022-07-14 Regular Meeting
draft: false
date: 2022-07-14T10:00:33.732Z
---
Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on 373 Middlefork Rd above Crouch

When: 14 July 2022, 10 AM

<!--more-->\[](https://www.gvfdidaho.com/media/2022-07-14-regular-meeting.pdf)

[Full Agenda (pdf)](https://www.gvfdidaho.com/media/2022-07-14-regular-meeting.pdf)