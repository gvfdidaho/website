---
title: 2022 June Auxiliary Meeting
draft: false
date: 2022-06-13T17:30:09.340Z
---
Auxiliary meeting and BBQ at a special location, new members are welcome. Contact Toni for more information.