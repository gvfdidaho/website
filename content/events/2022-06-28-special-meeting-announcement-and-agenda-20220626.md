---
title: 2022-06-28 Special Meeting Announcement and Agenda
draft: false
date: 2022-06-28T14:00:07.796Z
---
Special Meeting Announcement and Agenda.

Where: Main Station @ 373 Middlefork Rd, Crouch

When: June 28, 2022, 2 PM

<!--more-->

[Announcement and Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-28-special-meeting-exec-session-announcement-and-agenda.pdf)