---
title: 2022-11-15 Commissioners Meeting
draft: false
date: 2022-11-15T14:00:22.466Z
---
The GVFPD Fire Commissioners have changed the date of their regular, monthly meeting from November 8, 2022 to November 15, 2022.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 15 November 2022, 2:00 PM

[F﻿ull Agenda (pdf)](/media/2022-11-15-commagenda-revised.pdf)