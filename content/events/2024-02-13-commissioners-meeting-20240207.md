---
title: 2024-02-13 Commissioners Meeting
draft: false
date: 2024-02-13T14:00:11.550Z
---
The GVFPD Fire Commissioners meeting is scheduled for February 13 2024 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 13 February, 2024 at 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2024-02-13-regular-meeting-agenda.pdf)