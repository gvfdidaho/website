---
title: 2023-11-14 Regular Meeting Agenda
draft: false
date: 2023-11-14T14:00:47.819Z
---
The GVFPD Fire Commissioners meeting is scheduled for November 14, 2023 at 2pm.

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Regular Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 14 November 2023, 2:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-11-14-regular-meeting-agenda.pdf)