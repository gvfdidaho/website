---
title: 2022-06-16 Meeting Announcement & Agenda
draft: false
date: 2022-06-16T14:00:31.363Z
---
Garden Valley Fire Protection District Board of Commissioners

Regular (Rescheduled) Meeting Announcement & Agenda


Where: Main Fire Station on Middlefork Rd above Crouch

When: 16 June 2022, 2 PM

<!--more-->\[](https://www.gvfdidaho.com/media/2022-06-16-regular-meeting-agenda.pdf)

[Announcement & Agenda (pdf)](https://www.gvfdidaho.com/media/2022-06-16-regular-meeting-agenda.pdf)