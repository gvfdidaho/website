---
title: 2022-09-13 Regular Meeting Announcement
draft: false
date: 2022-09-13T14:00:44.224Z
---
Garden Valley Fire Protection District Board of Commissioners Regular Meeting Announcement & Agenda
Where: Main Fire Station on Middlefork Rd above Crouch
When: 13 September 2022, 2:00 PM

<!--more-->

[F﻿ull Agend (pdf)](https://www.gvfdidaho.com/media/2022-09-13-regular-meeting-agenda.pdf)