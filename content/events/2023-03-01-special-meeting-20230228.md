---
title: 2023-03-01 Special Meeting
draft: false
date: 2023-03-01T13:00:47.958Z
---
The GVFPD Fire Commissioners special meeting on 01 March 2023, 1:00 PM

<!--more-->

Garden Valley Fire Protection District Board of Commissioners

Special Meeting Announcement & Agenda

Where: Main Fire Station on Middlefork Rd above Crouch

When: 01 March 2023, 1:00 PM

[F﻿ull Agenda (pdf)](https://www.gvfdidaho.com/media/2023-03-01-special-mtg-agenda-and-announcement.pdf)