---
title: GVFPD Board of Commissioners Special Meeting 2022-05-24
draft: false
date: 2022-05-24T14:00:22.790Z
---
The GVFPD Board of Commissioners will be holding a special meeting on Tuesday, 05/24 at 2pm.

<!--more-->

[Full Agenda](https://www.gvfdidaho.com/media/2022-05-24-special-meeting-announcement-and-agenda.pdf) (pdf)