---
title: Motor Vehicle Accidents
picture: media/services_mva.jpg
---
Hwy 55 and Banks-Lowman roads are scenic byways along the Payette River system. They offer great views and easy distractions. High speeds, sharp curves, rock slides, ice, wildlife and other dangers are ever-present. MVAs are some of the toughest calls that GVFD responds to; our jobs is to bring order from chaos. Traffic control, environmental dangers, fire, hazmat, rescue and medical all in a single call. Drive Safe Garden Valley!