---
title: Medical EMS
picture: media/services_medical.jpg
---
We are a *fire* department with lots of *fire*fighters but the majority of our calls are medical emergencies. Garden Valley Fire Department is a combined Fire and EMS agency. Firefighters and EMTs cross-train and work together for the benefit of our citizens and patients. We operate as an ALS department with two volunteer Paramedics. We also have about 10 EMTs at the time of writing but this number fluctuates. We have 2 fully equipped ambulances.