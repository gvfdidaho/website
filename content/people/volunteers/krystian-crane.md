---
title: "Krystian Crane "
active: true
date: 2021-11-01T17:32:24.931Z
badge:
  - cpr
  - wildland
  - firefighter
  - rope
  - swiftwater
  - evoc
  - driver
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-krys-crane.jpg
---
I'm Krystian, I'm 28 years old and fought wildland fire for the last 5 years as my career. My grandparents and parents all live here full time. I'm now moving forward to learn and grow more in fire, and love being a part of this community.
