---
title: Phil Daigneau
active: true
date: 2023-12-06T18:00:00.000Z
badge:
  - cpr
  - emt
  - firefighter
  - evoc
  - hazmat
  - wildland
rank: 9. None
layout: volunteer
type: volunteer
image: /media/ic-people-missing-image.png
---
After moving to Idaho in 2021 we were blessed by finding our home in Garden Valley.  In April 2022 I started working at the Garden Valley Transfer Station and met many of the local resident that encouraged me to want to be more supportive of our community.  In December 2023 I had the opportunity to join the Garden Valley Fire Department and in March 2024 obtain my EMT license.  I look forward to learning from the extensive talent at the Fire Department.
