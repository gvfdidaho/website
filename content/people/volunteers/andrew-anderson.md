---
title: Andrew Anderson
active: false
date: 2021-03-01T17:25:36.506Z
badge:
  - cpr
  - evoc
  - wildland
  - firefighter
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-voluntters-andrew-anderson.jpg
---
Andrew is an Idaho native who grew up in Boise but has been camping, hunting, and fishing in Garden Valley most of his life. He and his family relocated to Garden Valley in 2020. Andrew's day job is a musician and producer. He has experience in EMS, Wildland fire and Corrections. He and his family are extremely thankful to be a part of this community and this department.