---
title: Trinia Richardson
active: true
date: 2020-10-01T16:31:28.088Z
badge:
  - cpr
  - emt
  - evoc
  - wildland
  - firefighter
  - hazmat
  - swiftwater
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-trinia-rishardsons.jpg
---
I started wildland fire fighting as a seasonal employee while gong to college, working as an engine boss and then on helitack. I became an EMT and volunteered with Elko County Ambulance and Fire Department. I met the love of my life, Chad, and took a break from EMS while raising our amazing family: Ambir, Tyler, Travis, Cheyanne and Clayton. We moved to Garden Valley in 2017 and I joined the department in 2020.I love this community and love contributing by doing something I truly love.
