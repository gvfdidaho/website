---
title: Deanna Paine
active: false
date: 2014-12-11T18:26:55.481Z
badge:
  - paramedic
  - acls
  - cpr
  - evoc
  - instructor
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-deanna-paine.jpg
---
I became an LPN in 2006. After I married Bud, I became interested in pre-hospital medicine and became an EMT in 2011. Took training during 2012 and obtained an AAS in Paramedicine/Emergency Medicine in 2013. I currently volunteer as a Paramedic with Garden Valley Fire and work part time as a Paramedic with Cascade Fire.