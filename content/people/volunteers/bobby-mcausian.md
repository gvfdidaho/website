---
title: Bobby McAusian
active: true
date: 2024-11-01T03:57:00.000Z
badge:
  - evoc
  - firefighter
rank: 9. None
layout: volunteer
type: volunteer
image: /media/bobby.jpg
---

Bobby comes with over 25 years of residential construction experience, 
he has always had a passion for helping others becoming an EMT in 2008. 
He and his wife bought land in Garden Valley in 2020, and have since 
built their forever home.
