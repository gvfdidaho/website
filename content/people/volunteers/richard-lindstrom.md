---
title: Richard Ross
active: true
date: 2022-06-01T14:30:52.279Z
badge:
  - firefighter
  - cpr
  - wildland
  - evoc
  - hazmat
rank: 9. None
layout: volunteer
type: volunteer
image: /media/richard.jpg
---
Richard has been with the fire department since June 2022. He has deep roots in Garden Valley and is honored to serve the community.
