---
title: Don Cobb
active: true
date: 2015-01-14T17:26:24.520Z
badge:
  - cpr
  - firefighter
  - structure
  - wildland
  - rope
  - evoc
  - driver
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-don-cobb.jpg
---
I spent over 20 years in the military and was awarded the Purple Heart. I have experience in law enforcement and fire service (CDA).
