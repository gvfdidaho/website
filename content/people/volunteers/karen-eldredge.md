---
title: Karen Eldredge
active: false
date: 2022-10-03T17:16:37.015Z
badge:
  - cpr
  - emt
  - hazmat
  - firefighter
  - swiftwater
rank: 9. None
layout: volunteer
type: volunteer
image: /media/886-karen-eldredge.jpg
---
I moved from Boise to Garden Valley in 2016, Pioneer Fire and Snowmageddon! I am a Volunteer/Reservist with Idaho Fish and Game and spend my summers volunteering to maintain trails in Idaho's wilderness.