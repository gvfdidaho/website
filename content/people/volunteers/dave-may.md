---
title: Dave May
active: false
date: 2021-01-11T16:40:34.549Z
badge:
  - emt
  - rope
  - hazmat
  - structure
  - wildland
  - evoc
  - engineer
rank: 3. Captain
layout: officer
type: volunteer
image: media/people-officers-dave-may.jpg
---
I was raised in Nampa, where, after finishing an enlistment in the Navy, I spent 30 years on the Nampa Fire Department. The last 20 were as a battalion chief, leading a shift of 25 firefighter/EMTs, and paramedics. From the time I was a young teenager, Garden Valley has been my stomping grounds. I’ve been married to Myra for 33 years, and our first date was to Silver Creek Plunge. This area has always held a special place in my heart and we were finally able to move up here full time on 2017. After retiring in June of 2019, I took some time off and then joined GVFD in January of 2021. I’m very proud of this department and the professionalism that its members exemplify, and I look forward to serving this community for many years to come!