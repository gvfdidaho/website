---
title: Michael McPherson
active: true
date: 2022-12-01T03:51:00.000Z
badge:
  - cpr
  - emt
  - evoc
  - firefighter
  - hazmat
  - wildland
  - driver
rank: 9. None
layout: volunteer
type: volunteer
image: /media/mike-1-.jpg
---

My wife and I moved here in 2021.  We are proud to be part of the Community
