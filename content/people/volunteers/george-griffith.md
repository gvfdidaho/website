---
title: George Griffith
active: true
date: 2021-01-19T17:06:17.180Z
badge:
  - emt
  - evoc
  - hazmat
  - rope
  - structure
  - swiftwater
  - wildland
  - cpr
  - driver
  - firefighter
rank: 3. Captain
layout: officer
type: volunteer
image: media/people-officers-george-griffith.jpg
---
I moved to Garden Valley in 2018 with my wife and we have since added two daughters to our family. Before moving to Garden Valley I had 11 years experience in the fire service and EMS. I joined GVFD in 2019.
