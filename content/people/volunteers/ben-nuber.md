---
title: Ben Nuber
active: true
date: 2020-03-05T04:10:19.721Z
badge:
  - emt
  - hazmat
  - wildland
  - evoc
  - cpr
  - firefighter
  - rope
  - structure
  - swiftwater
  - driver
rank: 4. Lieutenant
layout: officer
type: volunteer
image: media/people-volunteers-ben-nuber.jpg
---
Ben and his wife Kelly moved to Garden Valley in 2017. He joined the department in early 2020. Ben had no previous experience in fire or EMS but learned from the many amazing mentors and instructors in Garden Valley.
