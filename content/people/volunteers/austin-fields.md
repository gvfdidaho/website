---
title: Austin Fields
active: true
date: 2014-04-08T15:53:30.261Z
badge:
  - emt
  - hazmat
  - rope
  - structure
  - swiftwater
  - wildland
  - evoc
  - cpr
  - firefighter
  - driver
rank: 3. Captain
layout: officer
type: volunteer
image: media/people-officers-austin-fields.jpg
---
Austin Fields moved to Garden Valley in 2012 as a high school student. After graduating he began volunteering with GVFD in 2014. Austin Volunteered until 2018 when he was hired as a full-time firefighter and began working for GVFD professionally. Austin enjoys the wide variety of missions that GVFD faces and loves to continue training and knowledge to serve to the best of his ability. Austin loves Garden Valley and enjoys spending time with friends and family here. Austin hopes to continue to grow experience and relationships with the community here.
