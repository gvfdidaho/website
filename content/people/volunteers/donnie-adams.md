---
title: Donnie Adams
active: true
date: 2014-10-15T16:24:32.026Z
badge:
  - emt
  - cpr
  - hazmat
  - firefighter
rank: 9. None
layout: volunteer
type: volunteer
image: media/people-volunteers-donnie-adams.jpg
---
I am a volunteer with Garden Valley Fire Department. I started with Crouch Ambulance and have been volunteering for over 30 years.
