---
title: "Jenna Barron "
active: true
date: 2021-11-01T17:35:41.128Z
badge:
  - cpr
  - wildland
  - firefighter
  - emt
  - rope
  - structure
  - swiftwater
  - evoc
  - driver
  - hazmat
rank: 4. Lieutenant
layout: officer
type: volunteer
image: media/people-volunteers-jenna-baron.jpg
---
As a young firefighter I started off volunteering at my local fire department.
I then shifted to working fulltime as a Structual Firefighter for 3 years in Louisiana. Last year I moved to Idaho to fight wildland fires and broaden my horizons. I am now 22 years old and hope to continue expanding my knowledge and become a well rounded firefighter.
