---
title: Monthly Run Stats for {{date | date('YYYY-MM')}}
date: 2024-05-01T10:00:00.000Z
stat_medical: 12
stat_fire: 7
stat_other: 11
---
