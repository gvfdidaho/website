---
title: Burn Permits
date: 2022-01-01T14:55:59.613Z
image: ""
quicklink: true
---
![]()

Please obtain your free burn permit by clicking this link and selecting "New Permit"

[IDL Burn Permit Application (idaho.gov)](https://burnpermits.idaho.gov/)

The fire burn permit is free and required under Idaho law for any burning outside city limits statewide (excluding campfires) from May 10 through October 20. However, some cities and other jurisdictions - including local or county fire departments, Idaho DEQ, Tribal Reservations, and others - may have additional or alternate permit systems in place.

Boise County Burn Permits
The following tips should be followed when it is safe to conduct a debris burn:

Check with local authorities to make sure there are no local restrictions on burning currently in place, especially in cities and towns that have their own burning permit system.
GET A PERMIT. A permit is required
Notify your local fire department and neighbors to let them know your plans to burn
Do not burn on windy days
Stay abreast of changing weather conditions
Establish wide control lines down to bare mineral soil at least five feet wide around burn piles
Keep fire containment equipment on hand during the fire (e.g. rake, shovel, water)
Stay with the fire until it is completely out.