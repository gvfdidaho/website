---
title: Airmed Insurance
date: 2022-01-01T14:57:53.492Z
image: media/airmend_air-st-lukes.jpg
quicklink: true
---
Airmed insurance may be purchased through:

[Air St. Lukes Memebership](https://www.stlukesonline.org/health-services/service-groups/air-st-lukes)

**or**

[Life Flight Network](https://www.lifeflight.org/membership/)

Memberships are reciprical, only one puchase is necessary.